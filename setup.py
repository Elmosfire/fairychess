from setuptools import setup

with open("version") as file:
    version = "".join(file).strip()

setup(
    name="fairychess",
    version=version,
    description="A package to parse fairy chess pieces",
    url="https://gitlab.com/Elmosfire/fairychess/-/tree/main",
    author="Thorvald Dox",
    author_email="thorvalddx94@gmail.com",
    license="GPL3",
    packages=["fairychess"],
    install_requires=["numpy", "parsec", "matplotlib"],
    classifiers=[],
)
