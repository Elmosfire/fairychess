import unittest

from fairychess.fairychesspiece import sectant



class TestOrder(unittest.TestCase):
        
    def test_base(self):
        self.assertEqual(sectant(2,0), 0)
        self.assertEqual(sectant(2,1), 1)
        self.assertEqual(sectant(1,1), 2)
        self.assertEqual(sectant(1,2), 3)
        self.assertEqual(sectant(0,2), 4)
        self.assertEqual(sectant(-1,2), 5)
        self.assertEqual(sectant(-1,1), 6)
        self.assertEqual(sectant(-2,1), 7)
        self.assertEqual(sectant(-2,0), 8)
        self.assertEqual(sectant(-2,-1), 9)
        self.assertEqual(sectant(-1,-1), 10)
        self.assertEqual(sectant(-1,-2), 11)
        self.assertEqual(sectant(0,-2), 12)
        self.assertEqual(sectant(1,-2), 13)
        self.assertEqual(sectant(1,-1), 14)
        self.assertEqual(sectant(2,-1), 15)
        
        
if __name__ == "__main__":
    unittest.main()
