import unittest

from fairychess import parse_text

from itertools import product, chain, combinations




class TestParsingText(unittest.TestCase):
    def assertParseExact(self, text, value):
        self.assertEqual(parse_text(text), value, f"{text} -> {parse_text(text)} != {value}")
        
    def test_base(self):
        self.assertParseExact("W+", "a rook")



if __name__ == "__main__":
    unittest.main()
