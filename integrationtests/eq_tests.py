import unittest

from fairychess import parse_fairy

from itertools import product, chain, combinations


def powerset(iterable):
    "powerset([1,2,3]) → () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))


class TestParsingConsistensy(unittest.TestCase):
    def assertParseEqual(self, p1, p2):
        set1 = parse_fairy(p1).mvs
        set2 = parse_fairy(p2).mvs
        self.assertEqual(set1, set2, f"{p1} -> {set1} == {p2} -> {set2}")

    def assertParseNoOverlap(self, p1, p2):
        set1 = parse_fairy(p1).mvs
        set2 = parse_fairy(p2).mvs
        self.assertEqual(set1 & set2, frozenset(), f"{p1} -> {set1} <> {p2} -> {set2}")

    def assertParseIsSubset(self, p1, p2):
        set1 = parse_fairy(p1).mvs
        set2 = parse_fairy(p2).mvs
        self.assertEqual(
            len(set1 - set2), 0, f"{p1} -> {set1} is subset of {p2} -> {set2}"
        )

    def assertParseAllOrthodoxPieces(self, p1, p2, l=None, test=None):
        if l is None:
            l = p1.count("{}")
        if test is None:
            test = self.assertParseEqual

        for x in product("QRBNK", repeat=l):
            p1r = p1.format(*x)
            p2r = p2.format(*x)
            test(p1r, p2r)

    def assertParseAllOrthodoxPiecesNoOverlap(self, p1, p2, l=None):
        self.assertParseAllOrthodoxPieces(p1, p2, l=l, test=self.assertParseNoOverlap)

    def assertParseAllOrthodoxPiecesIsSubset(self, p1, p2, l=None):
        self.assertParseAllOrthodoxPieces(p1, p2, l=l, test=self.assertParseIsSubset)

    def test_direct(self):
        self.assertParseEqual("RB", "Q")
        self.assertParseEqual("W+", "R")
        self.assertParseEqual("F+", "B")
        self.assertParseEqual("FW", "K")
        self.assertParseEqual("WR", "R")
        self.assertParseEqual("FB", "B")

    def test_modifiers(self):
        self.assertParseAllOrthodoxPieces("fb{}", "v{}")
        self.assertParseAllOrthodoxPieces("lr{}", "s{}")
        self.assertParseAllOrthodoxPieces("vs{}", "{}")
        self.assertParseAllOrthodoxPieces("ffbb{}", "vv{}")
        self.assertParseAllOrthodoxPieces("llrr{}", "ss{}")
        self.assertParseAllOrthodoxPieces("vvss{}", "{}")

    def assert_modifiers_no_overlap(self, mod1, mod2):
        self.assertParseAllOrthodoxPiecesNoOverlap(f"{mod1}{{}}", f"{mod2}{{}}")
        self.assertParseAllOrthodoxPiecesNoOverlap(f"{mod1}{mod1}{{}}", f"{mod2}{{}}")
        self.assertParseAllOrthodoxPiecesNoOverlap(f"{mod1}{{}}", f"{mod2}{mod2}{{}}")
        self.assertParseAllOrthodoxPiecesNoOverlap(
            f"{mod1}{mod1}{{}}", f"{mod2}{mod2}{{}}"
        )

    def test_modifiers_no_overlap(self):
        self.assert_modifiers_no_overlap("f", "b")
        self.assert_modifiers_no_overlap("l", "r")

    def test_modifier_no_remove(self):
        for x in powerset(set("fblrvs")):
            for y in x:
                allmod = "".join(x)
                self.assertParseAllOrthodoxPiecesIsSubset(f"{y}{{}}", f"{allmod}{{}}")

    def test_boolean_operator_properties(self):
        self.assertParseAllOrthodoxPieces("!({}{})", "(!{})&(!{})")
        self.assertParseAllOrthodoxPieces("!({}&{})", "(!{})(!{})")
        self.assertParseAllOrthodoxPieces("{}&{}", "{1}&{0}")
        self.assertParseAllOrthodoxPieces("{}{}", "{1}{0}")
        self.assertParseAllOrthodoxPieces("({}{}){}", "{}({}{})")
        self.assertParseAllOrthodoxPieces("({}&{})&{}", "{}&({}&{})")
        self.assertParseAllOrthodoxPieces("({}{})&{}", "({0}&{2})({1}&{2})")
        self.assertParseAllOrthodoxPieces("({}&{}){}", "({0}{2})&({1}{2})")

    def test_leaper_subset_rider(self):
        for x in "WFDNAHCZG":
            self.assertParseIsSubset(f"{x}", f"{x}+")
            self.assertParseIsSubset(f"{x}", f"{x}2")
            self.assertParseIsSubset(f"{x}", f"{x}3")
            self.assertParseIsSubset(f"{x}", f"{x}4")
            self.assertParseEqual(f"{x}", f"{x}1")

    def test_fancy(self):
        self.assertParseEqual("sR", "sR-sR")
        self.assertParseNoOverlap("fR-fR", "fW")
        self.assertParseNoOverlap("F", "W")
        self.assertParseNoOverlap("Q", "N")
        self.assertParseNoOverlap("QN", "!QN")
        self.assertParseNoOverlap("ffN", "ssN")
        self.assertParseEqual("!(!(RB)&(!B))", "R")

    def test_coords(self):
        self.assertParseEqual(".10", ".1;0;")
        self.assertParseEqual(".25", ".2;5;")
        self.assertParseEqual(".25;2;", ".25;2")
        self.assertParseEqual(".25;2;", ".2;25;")
        self.assertParseEqual(".16", ".61")


if __name__ == "__main__":
    unittest.main()
