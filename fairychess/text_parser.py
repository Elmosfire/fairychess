from parsec import *
from functools import reduce


def detect_single_piece(code, name):
    @generate
    def parser():
        key = yield string(code)
        return name.lower()

    return parser

def detect_single_piece_as_rider(code, name):
    @generate
    def parser():
        key = yield string(code)
        return name.lower()

    return parser



PIECES_CHESS = {"R": "rook", "B": "bishop", "N": "knight", "Q": "queen", "K": "king"}

PIECES_SHOGI = {
    "RK": "dragon",
    "BK": "horse",
    "ffN": "knight",
    "fR": "lance",
    "FfW": "Silver General",
    "fB2": "Wood general",
    "fF": "Stone General",
    "fK": "Iron general",
}


PIECES_BASE = {
    "W": "vizier",
    "F": "witch",
    "D": "landship",
    "N": "knight",
    "A": "elephant",
    "H": "trebuchet",
    "C": "camel",
    "Z": "zebra",
    "G": "crow",
}

PIECES_EQ = {
    "W": ".01",
    "F": ".11",
    "D": ".02",
    "N": ".12",
    "A": ".22",
    "H": ".03",
    "C": ".13",
    "Z": ".23",
    "G": ".33",
}

PIECES_BASE_L = {
    "W+": "rook",
    "F+": "bishop",
    "D+": "Tank",
    "N+": "nightmare",
    "A+": "mahout",
    "H+": "howitzer",
    "C+": "mehariste",
    "Z+": "hauberk",
    "G+": "blackbird",
}

PIECES_OTHER = {
    ".12.34": "Impala",
    ".17.55": "Root-50-leaper",
    ".33.24.15": "Wyvern",
    ".41": "Giraffe",
    ".42": "Stag",
    ".43": "Antelope",
    ".50.34": "Root-25-Leaper",
    ".61": "Flamingo",
    "AC": "Roc",
    "ADGH": "Hawk",
    "B&(!F)": "picket",
    "B2": "Priest",
    "B4D": "B4nD",
    "BN+": "Banshee",
    "BW": "Rhino",
    "C-B": "Astrologer",
    "C.14": "Pegasus",
    "CZ": "Bison",
    "DNA": "Bear",
    "FL": "Wizard",
    "FN": "Dullahan",
    "HF": "Frog",
    "KAD": "Mammoth",
    "KC": "Gnu",
    "KN": "Centaur",
    "NA": "Kangaroo",
    "NB": "Cardinal",
    "NR": "Marchall",
    "QN": "Amazon",
    "QN+": "Night hag",
    "RF": "Sailor",
    "TH": "Toad",
    "W-B": "Manticore",
    "WD": "Machine",
    "WFAND": "Lion",
    "WfF": "Gold general",
    "Z+": "Zebrarider",
    "vRK": "Flying Stag",
    "ffNbssN": "Crab",
    "kr": "Admiral",
    "sWvD": "Donkey",
}

PIECES = PIECES_OTHER.copy()
PIECES.update(PIECES_BASE_L)
PIECES.update(PIECES_CHESS)
PIECES.update(PIECES_SHOGI)
PIECES.update(PIECES_BASE)


detect_piece_list = reduce(
    lambda x, y: x ^ y, [detect_single_piece(k, v) for k, v in PIECES.items()]
)

detect_piece_list_as_rider = reduce(
    lambda x, y: x ^ y, [detect_single_piece(k[:-1], v) for k, v in PIECES.items() if k[-1] == '+'] + [detect_single_piece(k, v+"rider") for k, v in PIECES.items()]
)



@generate
def add_rider_modifier():
    p = yield detect_piece_list_as_rider
    yield string("+")
    return p


@generate
def add_limited_rider_modifier():
    p = yield detect_piece_list_as_rider
    l = yield digit()
    return p + f" limited by {l} squares"


@generate
def modifier():
    m = yield regex("^[fblrsv]$")
    return dict(
        f="forward", b="backward", l="left", r="right", s="sideways", v="vertical"
    )[m]


@generate
def modlist():
    m = yield many1(modifier)
    return " or ".join(m) + " only"


@generate
def full_piece():
    m = yield modlist ^ string("")
    p = yield add_limited_rider_modifier ^ add_rider_modifier ^ detect_piece_list
    return "a " + m + (" " if m else "") + p


@generate
def expr():
    yield string("(")
    value = yield outward
    yield string(")")
    return value


piece_or_expr = full_piece ^ expr


@generate
def combine_or():
    values = yield many1(piece_or_expr)
    return " or ".join(values)


@generate
def combine_and():
    values = yield sepBy1(combine_or, string("&"))
    return " and ".join(values)


@generate
def combine_chain():
    values = yield sepBy1(combine_and, string("-"))
    return " followed by ".join(values)


@generate
def cert_not():
    yield string("!")
    value = yield combine_chain
    return "the inversion of " + value


maybe_not = cert_not ^ combine_chain


outward = maybe_not


def parse_text(text):
    return outward.parse_strict(text)


def export_dict_as_md_table(d):
    return "|Name|Code|\n|---|---|\n" + "\n".join(f"|{v}|{k}|" for k, v in d.items())

def export_leapers_as_md_table():
    return "|Name|Code|eqiuvalent|\n|---|---|---|\n" + "\n".join(f"|{v}|{k}|{PIECES_EQ[k]}|" for k, v in PIECES_BASE.items())

def export_riders_as_md_table():
    return "|Name|Leaper|Code|eqiuvalent|\n|---|---|---|---|\n" + "\n".join(f"|{v}|{PIECES_BASE[k[0]]}|{k}|{PIECES_EQ[k[0]] + '+'}|" for k, v in PIECES_BASE_L.items())


if __name__ == "__main__":
    with open("README.template") as file:
        base = "".join(file)

    base = base.replace("{{ chess }}", export_dict_as_md_table(PIECES_CHESS))
    base = base.replace("{{ shogi }}", export_dict_as_md_table(PIECES_SHOGI))
    base = base.replace("{{ other }}", export_dict_as_md_table(PIECES_OTHER))
    base = base.replace("{{ leapers }}", export_leapers_as_md_table())
    base = base.replace("{{ riders }}", export_riders_as_md_table())

    with open("README.md", "w") as file:
        file.write(base)
