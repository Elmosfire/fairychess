__package__ = "fairychess"

from itertools import product, chain


def sectant(x,y):
    assert x or y, "0,0 has no valid quadrant"
    if x > 0 and y == 0:
        return 0
    elif x > y > 0:
        return 1
    else:
        return sectant(x+y,-x+y) + 2

class FairyChessPiece:
    board_size = 100

    def __init__(self, data):
        self.mvs = frozenset(data)

    def copy(self):
        return type(self)(self.mvs)

    def adapt(self, nwset):
        return type(self)(nwset)

    def fix_symmetry(self):
        s = set(self.mvs)
        for x, y in self.mvs:
            for dx in (-1, 1):
                for dy in (-1, 1):
                    s.add((x * dx, y * dy))
                    s.add((y * dy, x * dx))
        return self.adapt(s)

    def fix_board(self):
        return self.adapt(
            {
                (x, y)
                for x, y in self.mvs
                if abs(x) < self.board_size and abs(y) < self.board_size
            }
        )

    def disallow_pass(self):
        return self.adapt({x for x in self.mvs if x != (0, 0)})

    @classmethod
    def leaper(cls, x, y):
        return cls([(x, y)]).fix_symmetry()

    def make_rider(self, r=0):
        s = set(self.mvs)
        if not r:
            r = self.board_size
        for i in range(2, r + 1):
            for x, y in self.mvs:
                s.add((x * i, y * i))
        return self.adapt(s).fix_board()
    
    def filter_sec(self, filter_):
        return self.adapt({(y, x) for (x, y) in self.mvs if (x or y) and sectant(x,y) in filter_})

    def forward_only(self, strong=False):
        if strong:
            return self.adapt({(x, y) for (x, y) in self.mvs if y <= -abs(x)})
        else:
            return self.adapt({(x, y) for (x, y) in self.mvs if y < 0})

    def xyflip(self):
        return self.adapt({(y, x) for (x, y) in self.mvs})

    def yflip(self):
        return self.adapt({(x, -y) for (x, y) in self.mvs})

    def left_only(self, strong=True):
        return self.xyflip().forward_only(strong).xyflip()

    def right_only(self, strong=True):
        return self.xyflip().yflip().forward_only(strong).yflip().xyflip()

    def backward_only(self, strong=True):
        return self.yflip().forward_only(strong).yflip()

    def vert_only(self, strong=True):
        return self.forward_only(strong) | self.backward_only(strong)

    def horiz_only(self, strong=True):
        return self.left_only(strong) | self.right_only(strong)

    def invert(self, _=True):
        return (
            self.adapt(
                {
                    (i, j)
                    for i in range(-self.board_size, self.board_size)
                    for j in range(-self.board_size, self.board_size)
                    if (i, j) not in self.mvs
                }
            )
            .fix_board()
            .disallow_pass()
        )

    @classmethod
    def rider(cls, x, y, z=0):
        return cls.hopper(x, y).make_rider(z)

    def __or__(self, other):
        return self.adapt(self.mvs | other.mvs)

    def __and__(self, other):
        return self.adapt(self.mvs & other.mvs)

    def __add__(self, other):
        return self.adapt(
            {(x1 + x2, y1 + y2) for (x1, y1) in self.mvs for (x2, y2) in other.mvs}
        )
        
    def __mul__(self, other):
        return self.adapt(
            {(x1 + x2, y1 + y2) for (x1, y1) in self.mvs for (x2, y2) in other.mvs if (x1,y1) == (0,0) or (x2,y2) == (0,0) or (sectant(x1,y1) - sectant(x2,y2)) % 16 in (0,1,15)}
        )
        
    def soft_chain(self, other):
        return self.adapt(
            {(x1 + x2, y1 + y2) for (x1, y1) in self.mvs for (x2, y2) in other.mvs if (x1,y1) == (0,0) or (x2,y2) == (0,0) or (sectant(x1,y1) - sectant(x2,y2)) % 16 in (0,1,2,14,15)}
        )
        
    def twist(self, other):
        return self.twist_left(other) | self.twist_right(other)
        
    def twist_left(self, other):
        return self.adapt(
            {(x1 + x2, y1 + y2) for (x1, y1) in self.mvs for (x2, y2) in other.mvs if (sectant(x1,y1) - sectant(x2,y2)) % 16 ==2}
        )
        
    def twist_right(self, other):
        return self.adapt(
        {(x1 + x2, y1 + y2) for (x1, y1) in self.mvs for (x2, y2) in other.mvs if (sectant(x1,y1) - sectant(x2,y2)) % 16 == 14}
    )
        
    def spiral(self):
        res = {}
        for x in self.mvs:
            assert sectant(*x) not in res, "cannot calculate spiral if sections have multiple moves"
            res[sectant(*x)] = x
        return res
        
        
    def rose_left_helper(self, other, start, invert=False, sharpness=2, flip=False):
        sp = other.spiral()
        x,y = start
        dx,dy = start
        index = sectant(x,y)
        yield x,y 
        while x or y:
            if not invert:
                index += 2
            else:
                index -= 2
            if flip:
                invert = not invert
            dx, dy = sp[index%16]
            x += dx
            y += dy
            yield x,y
            
    def rose_breach(self, filter_, invert=False, sharpness=2, flip=False):
        return self.adapt(chain(*(self.rose_left_helper(self, x,invert, sharpness, flip) for x in self.mvs if sectant(*x) in filter_)))
        
    
    def rose(self, filter_, sharpness=2, flip=False):
        return self.rose_breach(filter_, False, sharpness, flip) | self.rose_breach(filter_, True, sharpness, flip)
        
    def display(self):
        return '\n'.join(''.join(' x'[(x,-y) in self.mvs] if x or y else '#' for x in range(-7,8)) for y in range(-7,8))

    def moves(self, x, y):
        return self.__add__(self.adapt({(x, y)})).mvs
    

