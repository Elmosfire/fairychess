__package__ = "fairychess"

import numpy as np
from parsec import *
from functools import reduce

from .fairychesspiece import FairyChessPiece


number = (many1(digit()) << string(";")) ^ digit()


@generate
def leaper_exp():
    yield string(".")
    x = yield number
    y = yield number
    try:
        x = int("".join(x))
        y = int("".join(y))
    except ValueError:
        raise AssertionError
    return FairyChessPiece.leaper(x, y)


def parse_single_char_leaper(char, x, y):
    @generate
    def parser():
        yield string(char)
        return FairyChessPiece.leaper(x, y)

    return parser


def line_leaper(*chars):
    map_ = {}
    for c in chars:
        mx = len(c) - 1
        for i, x in enumerate(c):
            map_[x] = (i, mx)
    return reduce(
        lambda x, y: x ^ y, [parse_single_char_leaper(k, *v) for k, v in map_.items()]
    )


leaper = line_leaper("O", "WF", "DNA", "HCZG") ^ leaper_exp


@generate
def rider_limit():
    leaper_ = yield leaper
    x = yield digit()
    try:
        return leaper_.make_rider(int(x))
    except ValueError:
        raise ParseError


@generate
def rider_full():
    leaper_ = yield leaper
    x = yield string("+")
    try:
        return leaper_.make_rider(0)
    except ValueError:
        raise ParseError


rider_raw = rider_full ^ rider_limit


@generate
def rider_short():
    char_raw = yield regex("[BRQK]")
    return dict(
        B=rider_raw.parse("F+"),
        R=rider_raw.parse("W+"),
        Q=rider_raw.parse("F+") | rider_raw.parse("W+"),
        K=leaper.parse("W") | leaper.parse("F"),
    )[char_raw]


rider = rider_short ^ rider_raw

base = rider ^ leaper


@generate
def modifier():
    key = yield regex("[a-z]")
    s = yield string(key) | string("")
    try:
        return (
            dict(
                f=FairyChessPiece.forward_only,
                b=FairyChessPiece.backward_only,
                l=FairyChessPiece.left_only,
                r=FairyChessPiece.right_only,
                v=FairyChessPiece.vert_only,
                s=FairyChessPiece.horiz_only,
            )[key],
            bool(s),
        )
    except KeyError:
        raise AssertionError(
            f"{key} is not a valid modifier. Options are f,b,l,r,v and s"
        )


@generate
def piece():
    mods = yield many(modifier)
    b = yield base
    if not mods:
        return b
    else:
        return reduce(FairyChessPiece.__and__, [x(b, s) for x, s in mods])


@generate
def expr():
    yield string("(")
    value = yield outward
    yield string(")")
    return value


piece_or_expr = piece ^ expr


@generate
def combine_or():
    values = yield many1(piece_or_expr)
    return reduce(FairyChessPiece.__or__, values)


@generate
def combine_and():
    values = yield sepBy1(combine_or, string("&"))
    return reduce(FairyChessPiece.__and__, values)


@generate
def combine_chain():
    values = yield sepBy1(combine_and, string("-"))
    return reduce(FairyChessPiece.__add__, values)


@generate
def cert_not():
    yield string("!")
    value = yield combine_chain
    return FairyChessPiece.invert(value)


maybe_not = cert_not ^ combine_chain


outward = maybe_not


def parse_fairy(text):
    return outward.parse_strict(text).fix_board().disallow_pass()
