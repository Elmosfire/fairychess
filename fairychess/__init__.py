"""
fairychess.

A package to parse fairy chess pieces
"""

from .fairychesspiece import FairyChessPiece
from .parser import parse_fairy
from .text_parser import parse_text, ParseError

__version__ = "0.1.0"
__author__ = "Thorvald Dox"
