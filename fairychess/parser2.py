__package__ = "fairychess"

from email import generator
from tokenize import maybe
import numpy as np
from parsec import *
from functools import reduce
from operator import  and_, or_, add, mul

from .fairychesspiece import FairyChessPiece

def apply_local_operators(base, and_func, or_func, not_func, preproq=None, postproq=None):
    @generate
    def parse_wrapper():
        x = yield expr
        return x
    if preproq is not None:
        parse_preproq = preproq(parse_wrapper) ^ parse_wrapper
        base_preproq = preproq(base) ^ base
    else:
        parse_preproq = parse_wrapper
        base_preproq = base
    parse_and = parsecmap(sepBy1(parse_preproq, string("&")), lambda x: reduce(and_func, x))
    parse_or = parsecmap(sepBy1(parse_and, string("|")), lambda x: reduce(or_func, x))
    parse_not = parsecmap(string("!") >> parse_or, not_func) ^ parse_or
    if postproq is not None:
        parse_head = postproq(parse_not) ^ parse_not
    else:
        parse_head = parse_not
    parse_braces = string("(") >> parse_head << string(")")
    expr = base_preproq ^ parse_braces
    return parse_head

def fix_quadrant_set(rr):
    return set(r%16 for r in rr)

def build_modifier_parser():
    modifier_keysd = dict(
        a=0,
        d=2,
        r=4,
        q=6,
        v=8,
        p=10,
        l=12,
        b=14
    )

    modifier_keys_string = ''.join(x for x in modifier_keysd)
    assert modifier_keys_string == modifier_keys_string.lower()
    modifier_keys_string = modifier_keys_string.lower() + modifier_keys_string.upper()

    @generate
    def parse_modifier_key():
        x = yield one_of(modifier_keys_string)
        return modifier_keysd[x.lower()], x == x.upper()

    @generate
    def parse_modifier_set():
        start, startinc = yield parse_modifier_key
        stop, stopinc = yield parse_modifier_key
        if start < stop:
            rr = range(start+1, stop)
        elif start == stop:
            assert startinc==stopinc, "repeating modifier with different capitalisation is not valid."
            dist = 3 if startinc else 2
            return fix_quadrant_set(range(start-dist, stop+dist+1))
        else:
            rr = range(start+1, stop+16)
        rr = fix_quadrant_set(rr)
        if startinc:
            rr.add(start)
        if stopinc:
            rr.add(stop)
        return rr

    @generate
    def parse_modifier_single():
        start, startinc = yield parse_modifier_key
        if not startinc:
            return {start}
        else:
            return fix_quadrant_set(range(start-1, start+2))
        
    parse_default = parsecmap(string("O"), lambda x: fix_quadrant_set(range(16)))
    parse_modifier_free = parse_modifier_set ^ parse_modifier_single ^ parse_default



    def not_modifier(x):
        return set(range(16)) - x


        
    parse_modifier_head = apply_local_operators(parse_modifier_free, and_, or_, not_modifier)
    
    @generate
    def parse_modifier_mirror():
        p = yield parse_modifier_head
        addendum = yield one_of("-H+%*")
        if addendum == 'H':
            p = fix_quadrant_set(p | {16-x for x in p})
        elif addendum == '-':
            p = fix_quadrant_set(p | {8-x for x in p})
        elif addendum == '+':
            p = fix_quadrant_set(p | {16-x for x in p})
            p = fix_quadrant_set(p | {8-x for x in p})
        elif addendum == '%':
            p = fix_quadrant_set(p | {4+x for x in p})
            p = fix_quadrant_set(p | {8+x for x in p})
        elif addendum == '*':
            p = fix_quadrant_set(p | {8+x for x in p})
        return p


    return string("[") >> (parse_modifier_mirror ^ parse_modifier_head) << string("]")

def maybenumber(default=1):
    @generate
    def parser():
        x = yield many(digit()) 
        if not x:
            return default
        else:
            try:
                return int("".join(x))
            except ValueError:
                raise AssertionError
    return parser
            
@generate
def certainnumber():
    x = yield many1(digit()) 
    try:
        return int("".join(x))
    except ValueError:
        raise AssertionError

@generate
def leaper_free():
    x = yield maybenumber(1) 
    yield string("/")
    y = yield certainnumber
    return FairyChessPiece.leaper(x, y)

@generate
def leaper_bishop():
    yield string("x")
    y = yield maybenumber(1)
    return FairyChessPiece.leaper(y, y)

@generate
def leaper_rook():
    yield string("+")
    y = yield maybenumber(1)
    return FairyChessPiece.leaper(0, y)

leaper = leaper_free ^ leaper_bishop ^ leaper_rook

parse_modifier_operator = build_modifier_parser()

def modifier_helper_function(res):
    modifier, piece = res
    return piece.filter_sec(modifier)

def rider_helper_function(res):
    limit, piece = res
    print("rider", limit)
    return piece.make_rider(limit)


def roses_parser(prev):
    @generate
    def parser():
        yield string("@(")
        s = yield parse_modifier_operator ^ string("")
        if s == "":
            s = parse_modifier_operator.parse_strict("[O]")
        sharpness = yield maybenumber(2)
        direction = yield string('l') ^ string('r') ^ string("")
        zigzag = yield string('z') ^ string("")
        yield string(")")
        piece = yield prev
        if direction:
            return piece.rose_breach(s, direction == 'r', sharpness, zigzag)
        else:
            return piece.rose(s, sharpness, zigzag)
    return parser
        


def fairy_chess_operators(piece):
    piece_rider = parsecmap(maybenumber(0) + (string("*") >> piece) , rider_helper_function) ^ piece
    piece_maybe = parsecmap((string("?") >> piece_rider), lambda x: x | FairyChessPiece.leaper(0,0)) ^ piece_rider
    piece_modified = parsecmap(parse_modifier_operator + piece_maybe, modifier_helper_function) ^ piece_maybe
    return piece_modified

def fairy_chess_operators2(piece):
    parse_chain = parsecmap(sepBy1(piece, string("-")), lambda x: reduce(add, x))
    parse_chain_d = parsecmap(sepBy1(parse_chain, string("--")), lambda x: reduce(mul, x))
    parse_chain_f = parsecmap(sepBy1(parse_chain_d, string("~")), lambda x: reduce(FairyChessPiece.soft_chain, x))
    parse_rose = roses_parser(parse_chain_f) ^ parse_chain_f
    return parse_rose

parse_piece_operator =  apply_local_operators(leaper, and_, or_, FairyChessPiece.invert, fairy_chess_operators, fairy_chess_operators2)

print(parse_modifier_operator.parse_strict("[A]"))
#print(parse_piece_operator.parse_strict("[pl|A|]/2").display())
#print(parse_piece_operator.parse_strict("[VV&(!vv)|A]/2").display())
#print(parse_piece_operator.parse_strict("[LR|V](+|x)").display())
#print(parse_piece_operator.parse_strict("[!V-](+|x)").display())

print(parse_piece_operator.parse_strict("+~x").display())
