# FairyChess

A parser and calculator for fairy chess pieces.

## Getting started

Installation

install using

```
pip install git+https://gitlab.com/Elmosfire/fairychess.git
```

You can build any chess piece with

```python
from fairychess import parse_fairy, FairyChessPiece

queen = parse_fairy("Q")
knight = parse_fairy("K")
rook = parse_fairy("R")
```
The way to define peices is explained below

By default, only moves that are valid on a board of 100x100 squres or smaller are considered.
This limit can be changed by

```python
FairyChessPiece.boardsize = 1000
```

All possible moves as a frozen set of tuples can be retrived with

```python
from fairychess import parse_fairy, FairyChessPiece

queen = parse_fairy("Q")
queen.moves(3,3)
```

Where (3,3) is the coordinates from the current position.

### Fairy chess pieces.

The rest of this readme uses the terms leaper and rider.
Those are defined and explained here: 
https://en.wikipedia.org/wiki/Fairy_chess_piece#Leapers
https://en.wikipedia.org/wiki/Fairy_chess_piece#Riders

## Fairy pieces as mathematical objects

Fairy pieces are full mathematical objects

Leapers can be initialed with

```
knight = FairyChessPiece.leaper(2,1)
```
For example this initialises a knight

Riders can be initialed with

```
bishop = FairyChessPiece.rider(1,1)
```
For example this initialises a bishop.

An alternative way to initialise a rider is:
```
bishop = FairyChessPiece.leaper(1,1).make_rider()
```

To limit the range of a rider, one can at an argument

```
short_bishop = FairyChessPiece.rider(1,1, 4)
#or
short_bishop = FairyChessPiece.leaper(1,1).make_rider(4)
```

Pieces can be combined with a pipe symbol.

```
queen = rook | bishop
```

Then the new peice can use any move of the original piece

```
only_one_orthogonal = king & rook
```

If you use the ampersand, the resulting piece can only take move that where valid for both original pieces

If you sum pieces, the piece can only move like each individual piece in order.

So

```
strange_piece = king + rook
```

can only move like a king and then like a rook.
So it can go to any row and column next to or on the current row.

Not that this does not mean it can move like both individual pieces.


## The FairyPiece Language

The fairy chess piece language can be used to define almost every possible fairy chess piece.

The strings explained in the next section have to be given as agument to the `parse_fairy` function defined above.

This language is losely based on this notation: https://en.wikipedia.org/wiki/Betza%27s_funny_notation

### Leapers

A leaper can be written with a dot, with after it the digits of the movemnt in both directions
for example, a knight can be written as `.21`

If a leaper can move more than 10 squares in a direction you can end your number with a `;`

So a piece that moves 10 suares in one direction and 6 in the other would be
`.10;6` or `.10;6;`, as a single digit ended by a `;` is valid.
Note that `.6;10;` is valid but `.610;` is not.

Some leapers have a short notation of a single letter:


|Name|Code|eqiuvalent|
|---|---|---|
|vizier|W|.01|
|witch|F|.11|
|landship|D|.02|
|knight|N|.12|
|elephant|A|.22|
|trebuchet|H|.03|
|camel|C|.13|
|zebra|Z|.23|
|crow|G|.33|

The letters used are from Betza notation. But as many pieces are renamed here, the letters do not match the actual peices anymore.


### Riders

A rider is written as the equivalent leapers followed by a plus sign.

So for example a rook is a `W+` and a bishop is a `F+`

Some riders are hard coded as letters, as `R` is a rook, and `B` is a bisshop

|Name|Leaper|Code|eqiuvalent|
|---|---|---|---|
|rook|vizier|W+|.01+|
|bishop|witch|F+|.11+|
|Tank|landship|D+|.02+|
|nightmare|knight|N+|.12+|
|mahout|elephant|A+|.22+|
|howitzer|trebuchet|H+|.03+|
|mehariste|camel|C+|.13+|
|hauberk|zebra|Z+|.23+|
|blackbird|crow|G+|.33+|

### Combine pieces

Pieces that can do the moves of both pieces can be chained togther

So for example, a queen can be written as `RB` or `W+B+` or `.01+.11+`

Some combination pieces already have a notation, like `K` for the king (equivalent to `WF`), and `Q` for the queen.

### Other operations

the ampersand and chain operations from above can be written with `&` and `-`

A piece can be inverted with `!`

These operators support brackes, so `!(!(RB)&(!B))` is valid and will result in a piece equivalent to the rook.

Not that `!` has preference over `&` has preference over regular combining.

This means that `!R&K` is equivalent to `!(R&K)` and `R&!K` will raise a parse error.

### Directional modifier

Pieces (not expressions, only single pieces) can have a movement modifier.

|name|letter|
|---|---|
|forward|`f`|
|back|`b`|
|left|`l`|
|right|`r`|
|sideways|`s`|
|vertical|`v`|

The last two are just combinations.

Modifiers can be combined, so if a piece can move only forward and left, we can put `fl` before it.
Strong modifiers are defined by doubling up the modifier.

A weak forward modifier means that a piece can only move to places that are more forward (lower) in the y-direction, indepandant of the x direction.
A strong forward modigier means a piece can only move to places where the largest change in coordinates in the negative y direction.

For example, a knight with the weak forward modifier, `fN` can move to these fields:

![fN](http://www.chessvideos.tv/bimg/20bp4yy5puo0s.png)

While the knight with the strong forward modifier can  only move here:

![fN](http://www.chessvideos.tv/bimg/cf918vfayjpu.png)

## TODO

 - Pieces that capture different than other pieces
 - Pieces that start with different moves
 - Pieces that are limited to a part of the board
 - Royalty
 - Bend riders
 - Hoppers and Locusts

## Example fairy pieces

### Chess

|Name|Code|
|---|---|
|rook|R|
|bishop|B|
|knight|N|
|queen|Q|
|king|K|

### shogi

|Name|Code|
|---|---|
|dragon|RK|
|horse|BK|
|knight|ffN|
|lance|fR|
|Silver General|FfW|
|Wood general|fB2|
|Stone General|fF|
|Iron general|fK|

### other

|Name|Code|
|---|---|
|Impala|.12.34|
|Root-50-leaper|.17.55|
|Wyvern|.33.24.15|
|Giraffe|.41|
|Stag|.42|
|Antelope|.43|
|Root-25-Leaper|.50.34|
|Flamingo|.61|
|Roc|AC|
|Hawk|ADGH|
|picket|B&(!F)|
|Priest|B2|
|B4nD|B4D|
|Banshee|BN+|
|Rhino|BW|
|Astrologer|C-B|
|Pegasus|C.14|
|Bison|CZ|
|Bear|DNA|
|Wizard|FL|
|Dullahan|FN|
|Frog|HF|
|Mammoth|KAD|
|Gnu|KC|
|Centaur|KN|
|Kangaroo|NA|
|Cardinal|NB|
|Marchall|NR|
|Amazon|QN|
|Night hag|QN+|
|Sailor|RF|
|Toad|TH|
|Manticore|W-B|
|Machine|WD|
|Lion|WFAND|
|Gold general|WfF|
|Zebrarider|Z+|
|Flying Stag|vRK|
|Crab|ffNbssN|
|Admiral|kr|
|Donkey|sWvD|