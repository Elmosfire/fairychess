|Name|Code|Source|
|---|---|---|
|Alfil|A|other|
|Amazon|QN|other|
|Antelope|.43|other|
|Astrologer|C-B|other|
|B4nD|B4D|other|
|Banshee|BN+|other|
|Bear|DNA|other|
|Bison|CZ|other|
|Cardinal|NB|other|
|Centaur|KN|other|
|Crab|ffNbssN|other|
|Dabbabe|D|other|
|Donkey|sWfbD|other|
|Dullahan|FN|other|
|Elephant|B2|other|
|Flamingo|.61|other|
|Flying Stag|fbRK|other|
|Frog|HF|other|
|Giraffe|.41|other|
|Gnu|KC|other|
|Gold general|WfF|other|
|Hawk|ADGH|other|
|Impala|.12.34|other|
|Iron general|fK|shogi|
|Kangaroo|NA|other|
|Knightmare|N+|other|
|Lion|WFAND|other|
|Machine|WD|other|
|Mammoth|KAD|other|
|Manticore|W-B|other|
|Marchall|NR|other|
|Night hag|QN+|other|
|Pegasus|C.14|other|
|Rhino|BW|other|
|Roc|AC|other|
|Root-25-Leaper|.50.34|other|
|Root-50-leaper|.17.55|other|
|Sailor|RF|other|
|Silver General|FfW|shogi|
|Stag|.42|other|
|Stone General|fF|shogi|
|Threeleaper|H|other|
|Toad|TH|other|
|Trippler|G|other|
|Wizard|FL|other|
|Wood general|fB2|shogi|
|Wyvern|.33.24.15|other|
|Zebrarider|Z+|other|
|bishop|B|chess|
|camel|C|other|
|dragon|RK|shogi|
|ferz|F|other|
|horse|BK|shogi|
|king|K|chess|
|knight|N|chess|
|knight|ffN|shogi|
|lance|fR|shogi|
|picket|B&(!F)|other|
|queen|Q|chess|
|rook|R|chess|
|wazir|W|other|
|zebra|Z|other|
